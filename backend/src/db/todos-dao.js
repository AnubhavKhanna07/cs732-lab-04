/**
 * This file contains functions which interact with MongoDB, via mongoose, to perform Todo-related
 * CRUD operations.
 */

import { Todo } from "./todos-schema";

// TODO Exercise Three: Implement the five functions below.

export async function createTodo(todo) {
  const newTodo = new Todo(todo);
  newTodo.save();
  return newTodo;
}

export async function retrieveAllTodos() {
  const allTodos = await Todo.find();
  return allTodos;
}

export async function retrieveTodo(id) {
  await Todo.findById(id);
}

export async function updateTodo(todo) {
  const result = await Todo.findByIdAndUpdate(todo._id, todo, {
    new: true,
    useFindAndModify: false,
  });
  return result ? true : false;
}

export async function deleteTodo(id) {
  await Todo.deleteOne({ _id: id });
}
